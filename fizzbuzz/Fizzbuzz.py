def fizz(number):
    if number%3 == 0:
        return True
    else:
        return False

def buzz(number):
    if number%5 == 0:
        return True
    else:
        return False

def fizzbuzz(number):
    if fizz(number) and not(buzz(number)):
        return 'fizz'
    elif buzz(number) and not(fizz(number)):
        return 'buzz'
    elif buzz(number) and fizz(number):
        return 'fizzbuzz'
    else:
        return number
