import time

class tamagotchipet(object):
    def __init__(self):
        self.fullness = 50
        self.hunger = 50
        self.happiness = 50
        self.tiredness = 50
        self.age = 0
        self.aging()

    def aging(self):
        try:
            time.sleep(5)
            self.tiredness -= 5
            self.hunger += 5
            self.happiness -= 5
            self.age += 1
            print(self.age)
            self.aging()
        except(KeyboardInterrupt):
            break

    def get_hunger(self):
        return self.hunger

    def get_fullness(self):
        return self.fullness

    def get_happiness(self):
        return self.happiness

    def get_tiredness(self):
        return self.tiredness

    def set_hunger(self, number):
        self.hunger = number

    def set_fullness(self, number):
        self.fullness = number

    def feed(self):
        self.hunger -= 10
        self.fullness += 10

    def play(self):
        self.happiness += 10
        self.tiredness += 10

    def bed(self):
        self.tiredness -= 10

    def poop(self):
        self.fullness -= 10

if __name__ == '__main__':
    test_tamagotchi = tamagotchipet()
