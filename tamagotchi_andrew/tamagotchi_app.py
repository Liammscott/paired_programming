import time
from threading import Thread
import sys

class Pet(object):

    def __init__(self):
        self.__hunger = 50
        self.__fullness = 50
        self.__happiness = 50
        self.__tiredness = 50
        self.timenow = time.time()
        self.notice_time = time.time()
        self.age()
        pass

    def get_hungry(self):
        return self.__hunger

    def inspect_stats(self):
        print('Hunger value is %s, Fullness value is %s, Happliness value is %s, Tiredness value is %s' % (self.__hunger, self.__fullness, self.__happiness, self.__tiredness))

    def feed(self):
        self.__hunger -= 10
        self.__fullness += 10

    def play(self):
        self.__happiness += 10
        self.__tiredness += 10

    def put_to_bed(self):
        self.__tiredness -= 30

    def make_poop(self):
        self.__fullness -= 10

    def age(self):
        self.__tiredness += 10
        self.__hunger += 10
        self.__happiness -= 10


tamagotchi = Pet()


def fifteen_secs_passed():
    if time.time() - tamagotchi.notice_time >= 15:
        tamagotchi.notice_time = time.time()
        return True


def five_secs_passed():
    if time.time() - tamagotchi.timenow >= 5:
        return True


def run():
    print('\n get help by entering "help" ')
    while True:
        if fifteen_secs_passed():
            tamagotchi.timenow = time.time()
            tamagotchi.age()
        if tamagotchi.get_hungry() >= 70 and five_secs_passed():
            print('\n your pet is hungry!')
        if tamagotchi.get_hungry() >= 100:
            print('\n your pet is dead')
            break


def interact():
    while True:
        command = input('What\'s the plan:')
        if command == 'feed':
            tamagotchi.feed()
        if command == 'play':
            tamagotchi.play()
        if command == 'put to bed':
            tamagotchi.put_to_bed()
        if command == 'make poop':
            tamagotchi.make_poop()
        if command == 'inspect':
            tamagotchi.inspect_stats()
        if command == 'help':
            print('Try : feed, play, put to bed, make poop or inspect')
        # sys.stdin.empty()


if __name__ == '__main__':
    Thread(target=run).start()
    interact()

